Media Download Server scripts

This server access Usenet to download Movies and TV.

The first rule of Usenet is: DON'T talk about it. Keep this to yourself!

How to setup the server:

1. Install Ubuntu or Raspian Server. Download from a local (preferrably your ISP's unmetered) mirror.
2. Clone and edit the generic_server.conf file and replace "generic" with the hostname of the machine (e.g. myraspberrypibox_server.conf). Fill in each variable carefully to avoid errors.
3. Windows: Auto deploy by running build_server.bat. 
   Linux: 1. Upload the *.conf, *.sh, *.pub, *rsa files
          2. chmod +x *.sh
          3. sudo ./mds.sh 2>&1|tee mds.log
          4. sudo ./cleanup.sh 2>&1|tee cleanup.log
          5. sudo reboot
4. Check the logs and ensure there are no errors. They must have no errors or the system may not function as expected.         
5. Test by accessing the server via a browser (e.g. http://myraspberrypibox) A simple HTML page will display links to:
    1. Download Queue (Sabnzbd+)
    2. Movie Downloads (Couchpotato)
    3. TV Show Downloads (Sickbeard)
    4. Music Downloads (Headphones)
