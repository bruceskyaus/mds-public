@echo off
echo.
echo Automated build script for the MDS server
echo.
set /p HOSTNAME=Hostname: 
set /p USER=Admin user: 
set /p PWD=Password for %USER%@%HOSTNAME%: 

rem Get the name of the batch file path
set BATCHDIR=%~dp0

echo Preparing scripts for the MDS server %HOSTNAME%...
echo.

echo Checking login and SSH keys by connecting via plink, in case they change (user will be prompted to confirm)
plink -load %HOSTNAME% -l %USER% -pw %PWD% -ssh "echo Login successful."

echo Disabling suspend...
plink -load %HOSTNAME% -l %USER% -pw %PWD% -ssh "echo %PWD%|sudo -S systemctl mask sleep.target suspend.target hibernate.target hybrid-sleep.target 2>&1|tee suspend.log"

echo Uploading files...
psftp -l %USER% -pw %PWD% -batch -b "%BATCHDIR%\mds_upload.cmd" %HOSTNAME%

echo Converting scripts from DOS format to UNIX...
plink -load %HOSTNAME% -l %USER% -pw %PWD% -batch -m "%BATCHDIR%\mds_prep.cmd"

echo Building the server...
plink -load %HOSTNAME% -l %USER% -pw %PWD% -ssh "echo %PWD%|sudo -S ./mds.sh 2>&1|tee mds.log"

echo Cleaning up...
plink -load %HOSTNAME% -l %USER% -pw %PWD% -ssh "./cleanup.sh 2>&1|tee cleanup.log"

echo Rebooting the server...
plink -load %HOSTNAME% -l %USER% -pw %PWD% -ssh "echo %PWD%|sudo -S reboot" 2> nul

echo Done
pause
