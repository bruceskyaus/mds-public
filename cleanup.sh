#!/bin/bash

# Script 4: Automated cleanup of installation files

##################################################
# Functions
##################################################

function log () {
	printf "\n*** Logger: $(date "+%d/%m/%Y %T") : %s ***\n" "$1"
}

##################################################
# Cleanup
##################################################

log "Removing configuration files"
rm -f *_server*.conf
