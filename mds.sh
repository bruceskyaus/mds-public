#!/bin/bash

# Automated Media Download Server (MDS) Server software installation script

# Script 1: Automated installation of the following server applications:
# 1. Sabnzbd (download queue)
# 2. CouchPotato (movies)
# 3. SickBeard (tv shows)
# 4. Headphones (music)
# 5. Handbrake-CLI (video compression)
# 6. Nginx (web server)
# 7. UFW (firewall)
# 8. cron (automation)
# 9. Logrotate
# 10. cgroups (CPU prioritisation)

# OS: Ubuntu or Raspbian

##################################################
# Functions
##################################################

function log () {
	printf "\n*** Logger: $(date "+%d/%m/%Y %T") : %s ***\n" "$1"
}

function backup () {
	log "Backing up $1"
	sudo cp -p $1 "$1.$(date +"%Y%m%d")"
	sudo chmod a-w "$1.$(date +"%Y%m%d")"
}

function download () {
	log "Download $1 to $INSTALL_DOWNLOAD"
	sudo wget -q -P$INSTALL_DOWNLOAD $1
}

function set_config_value() {

	local NAME=$1
	local VALUE=$2
	local FILE=$3

    log "set_config_value: $NAME $VALUE $FILE"

    # Add escape characters to any forward slashes in the value
	VALUE=$(echo $VALUE | sed 's&\/&\\\/&g')

	sudo sed -i "s/^\($NAME[ \t]*=[ \t]*\).*$/\1$VALUE/" "$FILE"
}

function comment_line() {

    local LINE=$1
    local FILE=$2

    log "comment_line: $LINE $FILE"

    sudo sed -i "/^$LINE/ s/^#*/#/" "$FILE"
}

function add_after_line() {

    local LINE=$1
    local ADDED_LINE=$2
    local FILE=$3

    log "add_after_line: $LINE $ADDED_LINE $FILE"

    # Add escape characters to any forward slashes in the added line
    ADDED_LINE=$(echo $ADDED_LINE | sed 's&\/&\\\/&g')

    sudo sed -i "/^$LINE/a $ADDED_LINE" "$FILE"
}

function set_section_name_value () {

	local SECTION=$1
	local NAME=$2
	local VALUE=$3
	local FILE=$4

    log "set_section_name_value: $SECTION $NAME $VALUE $FILE"

    # Add escape characters to any forward slashes in the value
	VALUE=$(echo $VALUE | sed 's&\/&\\\/&g')

	sudo sed -i -e "/^\[$SECTION\]/,/^\[.*\]/s/^\($NAME[ \t]*=[ \t]*\).*$/\1$VALUE/" "$FILE"
}

##################################################
# Disable automatic upgrades
##################################################

log "Kill the unattended-upgrade process, if running"
sudo pkill -f unattended-upgrade

log "Reconfigure package manager"
sudo dpkg --configure -a

##################################################
# Get configuration parameters
##################################################
printf "Get configuration parameters from $(echo -e "$HOSTNAME")_server.conf\n"
if [ -f $(echo -e "$HOSTNAME")_server.conf ];
then
   source ./$(echo -e "$HOSTNAME")_server.conf
else
   echo "File $(echo -e "$HOSTNAME")_server.conf does not exist."
   exit 1
fi

##################################################
# Print configuration parameters
##################################################

printf "Software installation and configuration for $INSTALL_HOSTIP ($HOSTNAME.$INSTALL_DOMAIN)"
printf "\nScript environment:\n"
( set -o posix ; set ) | less

##################################################
# Sources
##################################################

log "Backup the apt sources list"
backup /etc/apt/sources.list

log "Update the apt sources list to $INSTALL_MIRROR_NAME mirror"
sudo cp /etc/apt/sources.list /etc/apt/sources.list.tmp

if [ $INSTALL_RASPBIAN -eq 1 ]
then
    log "Setting Raspbian mirror to $INSTALL_MIRROR_URL"
    sudo sed "s~mirrordirector.raspbian.org~$INSTALL_MIRROR_URL~g" /etc/apt/sources.list.tmp | sudo tee /etc/apt/sources.list
else
    log "Setting Ubuntu mirror to $INSTALL_MIRROR_URL"
    sudo sed "s~au.archive.ubuntu.com~$INSTALL_MIRROR_URL~g" /etc/apt/sources.list.tmp | sudo tee /etc/apt/sources.list
fi
sudo rm /etc/apt/sources.list.tmp

##################################################
# Host
##################################################

log "Building new hosts file /etc/hosts.tmp"
backup /etc/hosts

sudo echo "#<ip-address>    <hostname.domain.org>    <hostname>" >> /etc/hosts.tmp
sudo echo "127.0.0.1        localhost.localdomain	 localhost" >> /etc/hosts.tmp
sudo echo "127.0.1.1        $HOSTNAME" >> /etc/hosts.tmp
sudo echo "$INSTALL_HOSTIP     $HOSTNAME.$INSTALL_DOMAIN $HOSTNAME" >> /etc/hosts.tmp
sudo echo "" >> /etc/hosts.tmp
sudo echo "# The following lines are desirable for IPv6 capable hosts" >> /etc/hosts.tmp
sudo echo "::1     ip6-localhost ip6-loopback" >> /etc/hosts.tmp
sudo echo "fe00::0 ip6-localnet" >> /etc/hosts.tmp
sudo echo "ff00::0 ip6-mcastprefix" >> /etc/hosts.tmp
sudo echo "ff02::1 ip6-allnodes" >> /etc/hosts.tmp
sudo echo "ff02::2 ip6-allrouters" >> /etc/hosts.tmp

log "Overwriting /etc/hosts with /etc/hosts.tmp"
sudo mv -f /etc/hosts.tmp /etc/hosts

##################################################
# Change default login shell to bash
##################################################

log "Change default login shell to bash"
backup /etc/default/useradd
set_config_value "SHELL" "/bin/bash" /etc/default/useradd

if [ $INSTALL_RASPBIAN -eq 1 ]
then
    log "Add alias for ll=ls-la to .bashrc"
    (
        echo -e "# Custom aliases"
        echo -e "alias ll='ls -la'"
    ) > /home/$INSTALL_USER/.bash_aliases

    chown $INSTALL_USER:$INSTALL_USER /home/$INSTALL_USER/.bash_aliases
fi

##################################################
# Override the swap level
##################################################

log "Override the swap level"
backup /etc/sysctl.conf

(
	echo -e "\n";
	echo -e "# Override the swap level";
	echo -e "vm.swappiness=$INSTALL_SWAP_LEVEL";
) >> /etc/sysctl.conf

##################################################
# Disable plymouth (Ubuntu only)
##################################################

if [ $INSTALL_RASPBIAN -eq 0 ]
then
    if [ $INSTALL_PLYMOUTH_ENABLE -eq 0 ]
    then
        log "Disable Plymouth splash screen at boot"
        backup /etc/default/grub

        set_config_value "GRUB_CMDLINE_LINUX" "\"init=/sbin/init -v noplymouth INIT_VERBOSE=yes\"" /etc/default/grub
        sudo sed -i "s/^\#GRUB_TERMINAL.*/GRUB_TERMINAL=console/g" /etc/default/grub

        (
            echo -e "\n";
            echo -e "# Change to text boot only";
            echo -e "GRUB_GFXPAYLOAD_LINUX=text";
        ) >> /etc/default/grub

        sudo update-grub
    fi
fi

##################################################
# Users
##################################################

log "Create $INSTALL_SABNZBD_USER user"
sudo useradd -d /home/$INSTALL_SABNZBD_USER -m $INSTALL_SABNZBD_USER -s /bin/bash
echo -e "$INSTALL_SABNZBD_USER\n$INSTALL_SABNZBD_USER" | (sudo passwd $INSTALL_SABNZBD_PWD)

log "Create $INSTALL_SICKBEARD_USER user"
sudo useradd -M $INSTALL_SICKBEARD_USER -s /bin/bash
echo -e "$INSTALL_SICKBEARD_USER\n$INSTALL_SICKBEARD_USER" | (sudo passwd $INSTALL_SICKBEARD_PWD)

log "Create $INSTALL_COUCHPOTATO_USER user"
# sudo useradd -M $INSTALL_COUCHPOTATO_USER -s /bin/bash
# DEBUG: Couchpotato has a strange bug where it is trying to default the Movie Folders in the Movie Library Manager to /home/couchpotato
#        So create the folder and create a symlink to the /mnt directory
sudo useradd -d /home/$INSTALL_COUCHPOTATO_USER -m $INSTALL_COUCHPOTATO_USER -s /bin/bash
echo -e "$INSTALL_COUCHPOTATO_USER\n$INSTALL_COUCHPOTATO_USER" | (sudo passwd $INSTALL_COUCHPOTATO_PWD)

log "Create a symlink to /mnt for the Movie Library Manager"
sudo ln -s /mnt /home/$INSTALL_COUCHPOTATO_USER/mnt
sudo chown $INSTALL_SABNZBD_USER:$INSTALL_USER_GROUP /home/$INSTALL_COUCHPOTATO_USER/mnt

log "Create $INSTALL_HEADPHONES_USER user"
sudo useradd -M $INSTALL_HEADPHONES_USER -s /bin/bash
echo -e "$INSTALL_HEADPHONES_USER\n$INSTALL_HEADPHONES_USER" | (sudo passwd $INSTALL_HEADPHONES_PWD)

##################################################
# User groups
##################################################

log "Create the $INSTALL_USER_GROUP group"
sudo addgroup $INSTALL_USER_GROUP

log "Add $INSTALL_SABNZBD_USER to $INSTALL_USER_GROUP group"
sudo adduser $INSTALL_SABNZBD_USER $INSTALL_USER_GROUP

log "Modifying $INSTALL_SABNZBD_USER so that new files are owned by $INSTALL_SABNZBD_USER:$INSTALL_USER_GROUP group"
sudo usermod -g $INSTALL_USER_GROUP $INSTALL_SABNZBD_USER

log "Add $INSTALL_COUCHPOTATO_USER to $INSTALL_USER_GROUP group"
sudo adduser $INSTALL_COUCHPOTATO_USER $INSTALL_USER_GROUP

log "Add $INSTALL_SICKBEARD_USER to $INSTALL_USER_GROUP group"
sudo adduser $INSTALL_SICKBEARD_USER $INSTALL_USER_GROUP

log "Add $INSTALL_HEADPHONES_USER to $INSTALL_USER_GROUP group"
sudo adduser $INSTALL_HEADPHONES_USER $INSTALL_USER_GROUP

##################################################
# Copying bash aliases to other users
##################################################

if [ $INSTALL_RASPBIAN -eq 1 ]
then
    log "Copying /home/$INSTALL_USER/.bash_aliases to /home/$INSTALL_SABNZBD_USER"
    sudo cp -p /home/$INSTALL_USER/.bash_aliases /home/$INSTALL_SABNZBD_USER
    sudo chown $INSTALL_SABNZBD_USER:$INSTALL_USER_GROUP /home/$INSTALL_SABNZBD_USER/.bash_aliases

    log "Copying /home/$INSTALL_USER/.bash_aliases to /home/$INSTALL_COUCHPOTATO_USER"
    sudo cp -p /home/$INSTALL_USER/.bash_aliases /home/$INSTALL_COUCHPOTATO_USER
    sudo chown $INSTALL_COUCHPOTATO_USER:$INSTALL_COUCHPOTATO_USER /home/$INSTALL_COUCHPOTATO_USER/.bash_aliases
fi

##################################################
# Custom repositories
##################################################

log "Adding add-apt-repository command"
sudo DEBIAN_FRONTEND=noninteractive apt-get -y install software-properties-common

if [ $INSTALL_RASPBIAN -eq 0 ]
then
    log "Adding Ubuntu Handbrake repository"
    sudo add-apt-repository -y ppa:stebbins/handbrake-releases # broken?
    #sudo apt-add-repository -y ppa:stebbins/handbrake-snapshots
fi

##################################################
# Fix dependencies, update and upgrade packages - phase 1
##################################################

log "Fix dependencies - phase 1"
sudo DEBIAN_FRONTEND=noninteractive apt-get -y install

log "Updating packages (assume Yes to all questions) - phase 1"
sudo DEBIAN_FRONTEND=noninteractive apt-get -y update

log "Upgrading packages (assume Yes to all questions) - phase 1"
sudo DEBIAN_FRONTEND=noninteractive apt-get -y upgrade

##################################################
# Network
##################################################

log "Displaying available Ethernet interfaces"
ifconfig -a

log "Installing ethernet tool"
sudo DEBIAN_FRONTEND=noninteractive DEBIAN_FRONTEND=noninteractive apt-get -y install ethtool

log "Display ethernet modes for $INSTALL_NIC"
sudo ethtool $INSTALL_NIC

backup /etc/network/interfaces

log "Change the LAN speed to $INSTALL_LAN_SPEED"
sudo sed -i "s/^speed .*/speed $INSTALL_LAN_SPEED duplex full autoneg off/g" /etc/network/interfaces

log "Enable Wake-On-LAN"
sudo sed -i "s/^up ethtool .*/up ethtool -s $INSTALL_NIC wol g/g" /etc/network/interfaces

log "Harden the network"
(
	echo -e "# IP Spoofing protection"
	echo -e "net.ipv4.conf.all.rp_filter = 1"
	echo -e "net.ipv4.conf.default.rp_filter = 1"

	echo -e "# Ignore ICMP broadcast requests"
	echo -e "net.ipv4.icmp_echo_ignore_broadcasts = 1"

	echo -e "# Disable source packet routing"
	echo -e "net.ipv4.conf.all.accept_source_route = 0"
	echo -e "net.ipv6.conf.all.accept_source_route = 0"
	echo -e "net.ipv4.conf.default.accept_source_route = 0"
	echo -e "net.ipv6.conf.default.accept_source_route = 0"

	echo -e "# Ignore send redirects"
	echo -e "net.ipv4.conf.all.send_redirects = 0"
	echo -e "net.ipv4.conf.default.send_redirects = 0"

	echo -e "# Block SYN attacks"
	echo -e "net.ipv4.tcp_syncookies = 1"
	echo -e "net.ipv4.tcp_max_syn_backlog = 2048"
	echo -e "net.ipv4.tcp_synack_retries = 2"
	echo -e "net.ipv4.tcp_syn_retries = 5"

	echo -e "# Log Martians"
	echo -e "net.ipv4.conf.all.log_martians = 1"
	echo -e "net.ipv4.icmp_ignore_bogus_error_responses = 1"

	echo -e "# Ignore ICMP redirects"
	echo -e "net.ipv4.conf.all.accept_redirects = 0"
	echo -e "net.ipv6.conf.all.accept_redirects = 0"
	echo -e "net.ipv4.conf.default.accept_redirects = 0"
	echo -e "net.ipv6.conf.default.accept_redirects = 0"

	echo -e "# Ignore Directed pings"
	echo -e "net.ipv4.icmp_echo_ignore_all = 1"
) >> /etc/sysctl.conf

log "Reload sysctl"
sudo sysctl -p

log "Prevent IP spoofing"
(
	echo -e "nospoof on"
) >> /etc/host.conf

##################################################
# Security
##################################################

log "Securing shared memory"
backup /etc/fstab
(
	echo -e "tmpfs     /run/shm     tmpfs     defaults,noexec,nosuid     0     0"
) >> /etc/fstab

log "Disabling Debian banner"
backup /etc/ssh/sshd_config
(
	echo -e "\n# Disable Debian banner"
	echo -e "DebianBanner no"
) >> /etc/ssh/sshd_config

log "SSH: Moving RSA key to /home/$INSTALL_USER/.ssh/authorized_keys"
sudo mkdir -p /home/$INSTALL_USER/.ssh
sudo mv /home/$INSTALL_USER/id_rsa /home/$INSTALL_USER/.ssh
sudo mv /home/$INSTALL_USER/id_rsa.pub /home/$INSTALL_USER/.ssh/authorized_keys

log "SSH: Creating config file for git"
(
	echo -e "UserKnownHostsFile ~/.ssh/known_hosts"
) >> /home/$INSTALL_USER/.ssh/config

log "SSH: Setting ownership and permissions"
sudo chown -R $INSTALL_USER:$INSTALL_USER /home/$INSTALL_USER/.ssh
sudo chmod 700 /home/$INSTALL_USER/.ssh
sudo chmod 600 /home/$INSTALL_USER/.ssh/*

if [ $INSTALL_SSHKEY_LOGIN -eq 1 ]
then
	log "Disabling password authentication"
	(
		echo -e "\n# Disable password authentication"
		echo -e "PasswordAuthentication no"
	) >> /etc/ssh/sshd_config
fi

##################################################
# CIFS-UTILS
##################################################

log "Installing cifs-utils"
sudo DEBIAN_FRONTEND=noninteractive apt-get -y install cifs-utils

if [ $INSTALL_RASPBIAN -eq 0 ]
then
    log "Update the unmount in order to prevent CIFS from hanging during shutdown"
    sudo update-rc.d -f umountnfs.sh remove
    sudo update-rc.d umountnfs.sh stop 15 0 6 .
fi

# MDS
log "Creating mount directory: /mnt/$SMB_MOUNT_MDS_HOST/$SMB_MOUNT_MDS_SHARE"
sudo mkdir -p /mnt/$SMB_MOUNT_MDS_HOST/$SMB_MOUNT_MDS_SHARE

log "Changing owner to $INSTALL_SABNZBD_USER:$INSTALL_USER_GROUP for /mnt/$SMB_MOUNT_MDS_HOST"
sudo chown -R $INSTALL_SABNZBD_USER:$INSTALL_USER_GROUP /mnt/$SMB_MOUNT_MDS_HOST

# Movies
log "Creating mount directory: /mnt/$SMB_MOUNT_MOVIES_HOST/$SMB_MOUNT_MOVIES_SHARE"
sudo mkdir -p /mnt/$SMB_MOUNT_MOVIES_HOST/$SMB_MOUNT_MOVIES_SHARE

log "Changing owner to $INSTALL_SABNZBD_USER:$INSTALL_USER_GROUP for /mnt/$SMB_MOUNT_MOVIES_HOST"
sudo chown -R $INSTALL_SABNZBD_USER:$INSTALL_USER_GROUP /mnt/$SMB_MOUNT_MOVIES_HOST

# TV
log "Creating mount directory: /mnt/$SMB_MOUNT_TV_HOST/$SMB_MOUNT_TV_SHARE"
sudo mkdir -p /mnt/$SMB_MOUNT_TV_HOST/$SMB_MOUNT_TV_SHARE

log "Changing owner to $INSTALL_SABNZBD_USER:$INSTALL_USER_GROUP for /mnt/$SMB_MOUNT_TV_HOST"
sudo chown -R $INSTALL_SABNZBD_USER:$INSTALL_USER_GROUP /mnt/$SMB_MOUNT_TV_HOST

# Music
log "Creating mount directory: /mnt/$SMB_MOUNT_MUSIC_HOST/$SMB_MOUNT_MUSIC_SHARE"
sudo mkdir -p /mnt/$SMB_MOUNT_MUSIC_HOST/$SMB_MOUNT_MUSIC_SHARE

log "Changing owner to $INSTALL_SABNZBD_USER:$INSTALL_USER_GROUP for /mnt/$SMB_MOUNT_MUSIC_HOST"
sudo chown -R $INSTALL_SABNZBD_USER:$INSTALL_USER_GROUP /mnt/$SMB_MOUNT_MUSIC_HOST

# Additional configuration appended to the /etc/fstab file
backup /etc/fstab
sudo chmod 646 /etc/fstab
(
	echo -e "//$SMB_MOUNT_MDS_HOST/$SMB_MOUNT_MDS_SHARE /mnt/$SMB_MOUNT_MDS_HOST/$SMB_MOUNT_MDS_SHARE cifs username=$SMB_MOUNT_MDS_USER,password=$SMB_MOUNT_MDS_PWD,sec=ntlm,nounix,uid=$INSTALL_SABNZBD_USER,gid=$INSTALL_USER_GROUP,$SMB_MOUNT_IO,iocharset=$SMB_MOUNT_CHARSET,file_mode=$SMB_MOUNT_UMASK,dir_mode=$SMB_MOUNT_UMASK 0 0";

	echo -e "//$SMB_MOUNT_MOVIES_HOST/$SMB_MOUNT_MOVIES_SHARE /mnt/$SMB_MOUNT_MOVIES_HOST/$SMB_MOUNT_MOVIES_SHARE cifs username=$SMB_MOUNT_MOVIES_USER,password=$SMB_MOUNT_MOVIES_PWD,sec=ntlm,nounix,uid=$INSTALL_SABNZBD_USER,gid=$INSTALL_USER_GROUP,$SMB_MOUNT_IO,iocharset=$SMB_MOUNT_CHARSET,file_mode=$SMB_MOUNT_UMASK,dir_mode=$SMB_MOUNT_UMASK 0 0";
	echo -e "//$SMB_MOUNT_TV_HOST/$SMB_MOUNT_TV_SHARE /mnt/$SMB_MOUNT_TV_HOST/$SMB_MOUNT_TV_SHARE cifs username=$SMB_MOUNT_TV_USER,password=$SMB_MOUNT_TV_PWD,sec=ntlm,nounix,uid=$INSTALL_SABNZBD_USER,gid=$INSTALL_USER_GROUP,$SMB_MOUNT_IO,iocharset=$SMB_MOUNT_CHARSET,file_mode=$SMB_MOUNT_UMASK,dir_mode=$SMB_MOUNT_UMASK 0 0";
	echo -e "//$SMB_MOUNT_MUSIC_HOST/$SMB_MOUNT_MUSIC_SHARE /mnt/$SMB_MOUNT_MUSIC_HOST/$SMB_MOUNT_MUSIC_SHARE cifs username=$SMB_MOUNT_MUSIC_USER,password=$SMB_MOUNT_MUSIC_PWD,sec=ntlm,nounix,uid=$INSTALL_SABNZBD_USER,gid=$INSTALL_USER_GROUP,$SMB_MOUNT_IO,iocharset=$SMB_MOUNT_CHARSET,file_mode=$SMB_MOUNT_UMASK,dir_mode=$SMB_MOUNT_UMASK 0 0";
) >> /etc/fstab
sudo chmod 644 /etc/fstab

log "Mounting windows shares"
sudo mount -a

##################################################
# dos2unix
##################################################

log "Installing dos2unix"
sudo DEBIAN_FRONTEND=noninteractive apt-get -y install dos2unix

##################################################
# unzip
##################################################

log "Installing zip and unzip"
sudo DEBIAN_FRONTEND=noninteractive apt-get -y install zip unzip

##################################################
# Nginx
##################################################

if [ $NGINX_ENABLE -eq 1 ]
then
    log "Installing Nginx"
	sudo DEBIAN_FRONTEND=noninteractive apt-get -y install nginx

    if [ $NGINX_SELF_SIGNED_CERT -eq 1 ]
    then
        log "Create SSL certification directory"
        sudo mkdir $NGINX_SSL_CERT_DIR

        log "Generating the self signed certificate"
        sudo openssl req -new -newkey rsa:4096 -days $NGINX_SSL_DAYS -nodes -x509 -subj "/C=$NGINX_SSL_COUNTRY/ST=$NGINX_SSL_STATE/L=$NGINX_SSL_CITY/O=$NGINX_SSL_ORG/OU=Org/CN=$NGINX_DOMAIN" -keyout $NGINX_SSL_CERT_DIR/$NGINX_DOMAIN.key -out $NGINX_SSL_CERT

        log "Set owner and permissions"
        sudo chown -R root:root $NGINX_SSL_CERT_DIR
        sudo chmod -R 600 $NGINX_SSL_CERT_DIR

        log "Generating a key that does not have a password"
        sudo openssl rsa -in $NGINX_SSL_CERT_DIR/$NGINX_DOMAIN.key -out $NGINX_SSL_CERT_KEY
    fi

    log "Configuring the settings for the hardware"
    backup /etc/nginx/nginx.conf

    comment_line "worker_processes auto;" /etc/nginx/nginx.conf
    add_after_line "#worker_processes auto;" "worker_processes 1;" /etc/nginx/nginx.conf
    add_after_line "worker_processes 1;" "worker_priority $NGINX_WORKER_NICE_VALUE;" /etc/nginx/nginx.conf

    log "Disabling default config"
    sudo rm /etc/nginx/sites-enabled/default
    (
        echo -e "server {"
        if [ $NGINX_SSL_ENABLE -eq 1 ]
        then
            echo -e "\tlisten 443 ssl;"
        else
            echo -e "\tlisten 80;"
        fi

        echo -e ""
        echo -e "\tserver_name $NGINX_SERVER_NAME;"
        echo -e ""

        if [ $NGINX_SSL_ENABLE -eq 1 ]
        then
            echo -e "\t# Ignore any problems with finding a favicon"
            echo -e "\tlocation = /favicon.ico { access_log off; log_not_found off; }"
        fi

        echo -e "\tlocation / {"
        echo -e "\t\troot $INSTALL_INDEXPAGE_PATH/;"
        echo -e "\t}"
        echo -e ""

        if [ $NGINX_SSL_ENABLE -eq 1 ]
        then
            echo -e "\t# SSL config"
            echo -e "\tssl on;"
            echo -e "\tssl_certificate $NGINX_SSL_CERT;"
            echo -e "\tssl_certificate_key $NGINX_SSL_CERT_KEY;"
            echo -e ""
        fi
        echo -e "}"
    ) > /etc/nginx/sites-available/$NGINX_DOMAIN

    log "Enabling $NGINX_DOMAIN configuration file"
    sudo ln -s /etc/nginx/sites-available/$NGINX_DOMAIN /etc/nginx/sites-enabled/$NGINX_DOMAIN

    log "Testing Nginx configuration"
    sudo nginx -t

    log "Restarting nginx"
    sudo systemctl restart nginx.service
fi

##################################################
# git
##################################################

log "Installing git"
sudo DEBIAN_FRONTEND=noninteractive apt-get -y install git-core

##################################################
# Sabnzbdplus installation
##################################################

if [ $INSTALL_SABNZBD_ENABLE -eq 1 ]
then

    if [ $INSTALL_RASPBIAN -eq 1 ]
    then
        log "Added unrar-nonfree sources"
        echo "deb-src http://$INSTALL_MIRROR_URL/raspbian/ stretch main contrib non-free rpi" | sudo tee -a /etc/apt/sources.list

        log "Sync the apt database"
        sudo DEBIAN_FRONTEND=noninteractive apt-get -y update

        log "Create a temporary directory for unrar-nonfree"
        cd $(mktemp -d)

        log "Install the dependencies required by unrar-nonfree"
        sudo DEBIAN_FRONTEND=noninteractive apt-get -y build-dep unrar-nonfree

        log "Download the unrar-nonfree sources and build the .deb package"
        sudo DEBIAN_FRONTEND=noninteractive apt-get -y source -b unrar-nonfree

        log "Install the generated .deb package. Its name varies depending on the version of unrar-nonfree"
        sudo dpkg -i unrar*.deb

        log "Installing sabnzbdplus dependencies"
        sudo DEBIAN_FRONTEND=noninteractive apt-get -y install python-gdbm python-cheetah python-openssl dirmngr

        log "Added sabnzbdplus sources"
        echo "deb http://ppa.launchpad.net/jcfp/ppa/ubuntu precise main" | sudo tee -a /etc/apt/sources.list
        sudo apt-key adv --keyserver hkp://pool.sks-keyservers.net:11371 --recv-keys 0x98703123E0F52B2BE16D586EF13930B14BB9F05F
    fi

	log "Installing sabnzbdplus dependencies"
    sudo DEBIAN_FRONTEND=noninteractive apt-get -y install python-pip
    pip install sabyenc --upgrade

	log "Installing sabnzbdplus"
    sudo DEBIAN_FRONTEND=noninteractive apt-get -y install sabnzbdplus

    if [ $INSTALL_RASPBIAN -eq 1 ]
    then
        log "Add PAR2 Multicore dependencies (must be installed after sabnzbdplus to replace original par2)"
        sudo DEBIAN_FRONTEND=noninteractive apt-get -y install devscripts build-essential debhelper dh-autoreconf libwww-perl libtbb-dev

        log "Installing PAR2 Multicore"
        log "Create a temporary directory"
        cd $(mktemp -d)
        git clone https://github.com/jcfp/debpkg-par2tbb.git
        cd debpkg-par2tbb
        uscan --force-download
        dpkg-buildpackage -S -us -uc -d
        dpkg-source -x ../par2cmdline-tbb_*.dsc
        rm par2cmd*.orig.tar.gz
        cd par2cmd*
        ./configure --build=arm-linux-gnu --prefix=/usr --includedir=${prefix}/include --mandir=${prefix}/share/man --infodir=${prefix}/share/info --sysconfdir=/etc --localstatedir=/var --disable-silent-rules --libdir=${prefix}/lib/arm-linux-gnu --libexecdir=${prefix}/lib/arm-linux-gnu
        sudo make -j2
        sudo make install
    fi

	##################################################
	# Fix dependencies, update and upgrade packages - phase 2
	##################################################

	log "Fix dependencies - phase 2"
	sudo DEBIAN_FRONTEND=noninteractive apt-get -y install

	log "Updating packages (assume Yes to all questions) - phase 2"
	sudo DEBIAN_FRONTEND=noninteractive apt-get -y update

	log "Upgrading packages (assume Yes to all questions) - phase 2"
	sudo DEBIAN_FRONTEND=noninteractive apt-get -y upgrade

	##################################################
	# sabnzbdplus configuration
	##################################################

	backup /etc/default/sabnzbdplus

	set_config_value "USER" "$INSTALL_SABNZBD_USER" /etc/default/sabnzbdplus
	set_config_value "HOST" "$INSTALL_SABNZBD_HOST" /etc/default/sabnzbdplus
	set_config_value "PORT" "$INSTALL_SABNZBD_PORT" /etc/default/sabnzbdplus

	sudo chmod +x /etc/init.d/sabnzbdplus
	sudo service sabnzbdplus start

	# log "Creating download directories"
	# sudo mkdir -p /var/media/Downloads
	# sudo mkdir /var/media/Downloads/backup
	# sudo mkdir /var/media/Downloads/complete
	# sudo mkdir /var/media/Downloads/incomplete
	# sudo mkdir /var/media/Downloads/tmp

	log "Creating script directories"
	sudo mkdir -p /home/$INSTALL_SABNZBD_USER/bin
	sudo mkdir -p /home/$INSTALL_SABNZBD_USER/scripts
	# sudo chown -R $INSTALL_SABNZBD_USER:$INSTALL_USER_GROUP /var/media
	sudo chown -R $INSTALL_SABNZBD_USER:$INSTALL_USER_GROUP /home/$INSTALL_SABNZBD_USER/scripts

	log "Moving disk full delete script to /home/$INSTALL_SABNZBD_USER/bin and setting owner/permissions"
	sudo mv /home/$INSTALL_USER/remove_downloads_disk_full.sh /home/$INSTALL_SABNZBD_USER/bin
	sudo chown -R $INSTALL_SABNZBD_USER:$INSTALL_USER_GROUP /home/$INSTALL_SABNZBD_USER/bin
	sudo chmod +x /home/$INSTALL_SABNZBD_USER/bin/remove_downloads_disk_full.sh

	# log "Setting permissions for group read/write access to /var/media/Downloads"
	# sudo chmod -R 775 /var/media/Downloads

	log "Stopping sabnzbdplus - unconfigured (doesn't matter if not running)"
	sudo service sabnzbdplus stop

	log "Start sabnzbdplus again to create /home/$INSTALL_SABNZBD_USER/.sabnzbd/sabnzbd.ini file"
	sudo service sabnzbdplus start
	sleep 15s

	log "Stopping sabnzbdplus again"
	sudo service sabnzbdplus stop

	log "Configuring sabnzbdplus"
	backup /home/$INSTALL_SABNZBD_USER/.sabnzbd/sabnzbd.ini

	# Capture the sabnzbd api key
	export SABNZBD_API_KEY=$(sed -n 's/^api_key *= *\([^ ]*.*\)/\1/p' /home/$INSTALL_SABNZBD_USER/.sabnzbd/sabnzbd.ini)
	log "sabnzbdplus Api Key is $SABNZBD_API_KEY"

	# Capture the sabnzbd nzb key
	export SABNZBD_NZB_KEY=$(sed -n 's/^nzb_key *= *\([^ ]*.*\)/\1/p' /home/$INSTALL_SABNZBD_USER/.sabnzbd/sabnzbd.ini)
	log "sabnzbdplus NZB Key is $SABNZBD_NZB_KEY"

	set_section_name_value "misc" "cache_limit" "$INSTALL_SABNZBD_CACHE_LIMIT" /home/$INSTALL_SABNZBD_USER/.sabnzbd/sabnzbd.ini
	set_section_name_value "misc" "dirscan_dir" "/mnt/$SMB_MOUNT_MDS_HOST/$SMB_MOUNT_MDS_SHARE/Downloads" /home/$INSTALL_SABNZBD_USER/.sabnzbd/sabnzbd.ini
	set_section_name_value "misc" "permissions" "775" /home/$INSTALL_SABNZBD_USER/.sabnzbd/sabnzbd.ini
	set_section_name_value "misc" "download_free" "1G" /home/$INSTALL_SABNZBD_USER/.sabnzbd/sabnzbd.ini
	set_section_name_value "misc" "nzb_backup_dir" "/mnt/$SMB_MOUNT_MDS_HOST/$SMB_MOUNT_MDS_SHARE/Downloads/backup" /home/$INSTALL_SABNZBD_USER/.sabnzbd/sabnzbd.ini
	set_section_name_value "misc" "ignore_samples" "2" /home/$INSTALL_SABNZBD_USER/.sabnzbd/sabnzbd.ini
	set_section_name_value "misc" "download_dir" "/mnt/$SMB_MOUNT_MDS_HOST/$SMB_MOUNT_MDS_SHARE/Downloads/incomplete" /home/$INSTALL_SABNZBD_USER/.sabnzbd/sabnzbd.ini
	set_section_name_value "misc" "complete_dir" "/mnt/$SMB_MOUNT_MDS_HOST/$SMB_MOUNT_MDS_SHARE/Downloads/complete" /home/$INSTALL_SABNZBD_USER/.sabnzbd/sabnzbd.ini
	set_section_name_value "misc" "no_dupes" "1" /home/$INSTALL_SABNZBD_USER/.sabnzbd/sabnzbd.ini
	set_section_name_value "misc" "tv_sort_string" "%sn/Season %s/%sn - S%0sE%0e - %en.%ext" /home/$INSTALL_SABNZBD_USER/.sabnzbd/sabnzbd.ini
	set_section_name_value "misc" "script_dir" "/home/$INSTALL_SABNZBD_USER/scripts" /home/$INSTALL_SABNZBD_USER/.sabnzbd/sabnzbd.ini
	set_section_name_value "misc" "fail_on_crc" "1" /home/$INSTALL_SABNZBD_USER/.sabnzbd/sabnzbd.ini
	set_section_name_value "misc" "folder_max_length" "64" /home/$INSTALL_SABNZBD_USER/.sabnzbd/sabnzbd.ini
	set_section_name_value "misc" "quota_period" "$INSTALL_QUOTA_PERIOD" /home/$INSTALL_SABNZBD_USER/.sabnzbd/sabnzbd.ini
	set_section_name_value "misc" "quota_day" "$INSTALL_QUOTA_DAY" /home/$INSTALL_SABNZBD_USER/.sabnzbd/sabnzbd.ini
	set_section_name_value "misc" "quota_size" "$INSTALL_QUOTA_SIZE" /home/$INSTALL_SABNZBD_USER/.sabnzbd/sabnzbd.ini
	set_section_name_value "misc" "quota_resume" "$INSTALL_QUOTA_RESUME" /home/$INSTALL_SABNZBD_USER/.sabnzbd/sabnzbd.ini
	set_section_name_value "misc" "par_option" "$INSTALL_SABNZBD_PAR_OPTION" /home/$INSTALL_SABNZBD_USER/.sabnzbd/sabnzbd.ini
	set_section_name_value "misc" "nice" "$INSTALL_SABNZBD_NICE" /home/$INSTALL_SABNZBD_USER/.sabnzbd/sabnzbd.ini
	set_section_name_value "misc" "ionice" "$INSTALL_SABNZBD_IONICE" /home/$INSTALL_SABNZBD_USER/.sabnzbd/sabnzbd.ini

    # Set the schedule to pause all NZBs at 6:00pm and resume at midnight
	set_section_name_value "misc" "schedlines" "\"1 0 18 1234567 pause_all \", \"1 0 0 1234567 resume \"" /home/$INSTALL_SABNZBD_USER/.sabnzbd/sabnzbd.ini

	# Set newzbin username and password
	set_section_name_value "newzbin" "username" "$NEWS_NEWZBIN_USER" /home/$INSTALL_SABNZBD_USER/.sabnzbd/sabnzbd.ini
	set_section_name_value "newzbin" "password" "$NEWS_NEWZBIN_PWD" /home/$INSTALL_SABNZBD_USER/.sabnzbd/sabnzbd.ini

	# Additional configuration appended to the sabnzbd.ini file
	(
		# Server configuration
		echo "[servers]";

		# Primary news server
		echo "[[$NEWS_SERVER_PRIMARY_HOST]]";
		echo "username = \"$NEWS_SERVER_PRIMARY_USER\"";
		echo "enable = $NEWS_SERVER_PRIMARY_ENABLE";
		echo "name = \"$NEWS_SERVER_PRIMARY_NAME\"";
		echo "fillserver = 0";
		echo "connections = $NEWS_SERVER_PRIMARY_CONNECTIONS";
		echo "ssl = $NEWS_SERVER_PRIMARY_SSL_ENABLE";
		echo "host = $NEWS_SERVER_PRIMARY_HOST";
		echo "timeout = $NEWS_SERVER_PRIMARY_TIMEOUT";
		echo "password = \"$NEWS_SERVER_PRIMARY_PWD\"";
		echo "optional = $NEWS_SERVER_PRIMARY_OPTIONAL_ENABLE";
		echo "port = $NEWS_SERVER_PRIMARY_PORT";
		echo "retention = $NEWS_SERVER_PRIMARY_RETENTION";

		# Secondary news server
		echo "[[$NEWS_SERVER_SECONDARY_HOST]]";
		echo "username = \"$NEWS_SERVER_SECONDARY_USER\"";
		echo "enable = $NEWS_SERVER_SECONDARY_ENABLE";
		echo "name = \"$NEWS_SERVER_SECONDARY_NAME\"";
		echo "fillserver = 1"; # Backup server
		echo "connections = $NEWS_SERVER_SECONDARY_CONNECTIONS";
		echo "ssl = $NEWS_SERVER_SECONDARY_SSL_ENABLE";
		echo "host = $NEWS_SERVER_SECONDARY_HOST";
		echo "timeout = $NEWS_SERVER_SECONDARY_TIMEOUT";
		echo "password = \"$NEWS_SERVER_SECONDARY_PWD\"";
		echo "optional = $NEWS_SERVER_SECONDARY_OPTIONAL_ENABLE";
		echo "port = $NEWS_SERVER_SECONDARY_PORT";
		echo "retention = $NEWS_SERVER_SECONDARY_RETENTION";

		# Download categories;
		echo "[categories]";

		# Default category
		echo "[[*]]";
		echo "priority = 0";
		echo "pp = 3";
		echo "name = *";
		echo "script = None";
		echo "newzbin = \"\"";
		echo "dir = \"\"";

		# TV Category
		echo "[[tv]]";
		echo "priority = 1";
		echo "pp = 3";
		echo "name = tv";
		echo "script = sabToSickBeard.py";
		echo "newzbin = tv";
		# echo "dir = /var/media/Downloads/complete";
		echo "dir = /mnt/$SMB_MOUNT_MDS_HOST/$SMB_MOUNT_MDS_SHARE/Downloads/complete";

		# Movies category
		echo "[[movies]]";
		echo "priority = 0";
		echo "pp = 3";
		echo "name = movies";
		echo "script = None";
		echo "newzbin = Movies.*";
		# echo "dir = /var/media/Downloads/complete";
		echo "dir = /mnt/$SMB_MOUNT_MDS_HOST/$SMB_MOUNT_MDS_SHARE/Downloads/complete";

		# Documentaries category
		echo "[[documentaries]]";
		echo "priority = 0";
		echo "pp = 3";
		echo "name = documentaries";
		echo "script = None";
		echo "newzbin = Documentaries.*";
		# echo "dir = /var/media/Downloads/complete";
		echo "dir = /mnt/$SMB_MOUNT_MDS_HOST/$SMB_MOUNT_MDS_SHARE/Downloads/complete";

		# Documentaries category
		echo "[[music]]";
		echo "priority = 0";
		echo "pp = 3";
		echo "name = music";
		echo "script = None";
		echo "newzbin = Music.*";
		# echo "dir = /var/media/Downloads/complete";
		echo "dir = /mnt/$SMB_MOUNT_MDS_HOST/$SMB_MOUNT_MDS_SHARE/Downloads/complete";
	) >> /home/$INSTALL_SABNZBD_USER/.sabnzbd/sabnzbd.ini

	log "Deleting default Downloads directory /home/$INSTALL_SABNZBD_USER/Downloads"
	sudo rm -r /home/$INSTALL_SABNZBD_USER/Downloads

	log "Starting sabnzbdplus - unconfigured (doesn't matter if it fails)"
	sudo service sabnzbdplus start

	log "Restarting sabnzbdplus"
	sudo service sabnzbdplus restart
fi

##################################################
# CouchPotato
##################################################

if [ $INSTALL_COUCHPOTATO_ENABLE -eq 1 ]
then
	log "Installing CouchPotato"
	sudo git clone https://github.com/RuudBurger/CouchPotatoServer.git /opt/couchpotato
	sudo chown -R $INSTALL_COUCHPOTATO_USER:$INSTALL_COUCHPOTATO_USER /opt/couchpotato

	log "Setup CouchPotato as a service"
	sudo cp /opt/couchpotato/init/ubuntu /etc/init.d/couchpotato
	sudo cp /opt/couchpotato/init/ubuntu.default /etc/default/couchpotato

	backup /etc/default/couchpotato

	log "Create CouchPotato data directory"
	sudo mkdir /var/couchpotato
	sudo chown -R $INSTALL_COUCHPOTATO_USER:$INSTALL_COUCHPOTATO_USER /var/couchpotato

	## CP_USER= #$RUN_AS, username to run couchpotato under, the default is couchpotato
	## CP_HOME= #$APP_PATH, the location of couchpotato.py, the default is /opt/couchpotato
	## CP_DATA= #$DATA_DIR, the location of couchpotato.db, cache, logs, the default is /var/couchpotato
	## CP_PIDFILE= #$DATA_DIR, the location of the pid file, the default is /var/run/couchpotato.pid (requires root privileges)

	set_config_value "CP_HOME" "/opt/couchpotato" /etc/default/couchpotato
	set_config_value "CP_USER" "$INSTALL_COUCHPOTATO_USER" /etc/default/couchpotato

	# Additional configuration appended to the /etc/default/couchpotato file
	sudo chmod 646 /etc/default/couchpotato
	(
		echo -e "\n";
		echo -e "CP_DATA=/var/couchpotato";
		echo -e "CP_PIDFILE=/var/run/couchpotato/couchpotato.pid";
	) >> /etc/default/couchpotato
	sudo chmod 644 /etc/default/couchpotato

	log "Set CouchPotato to run on boot"
	sudo update-rc.d couchpotato defaults

	##################################################
	# CouchPotato configuration
	##################################################
	log "Starting CouchPotato (creates default configuration)"
	sudo service couchpotato start
	sleep 15s

	log "Stopping CouchPotato"
	sudo service couchpotato stop

	log "Configuring CouchPotato"
	backup /var/couchpotato/settings.conf

	export INSTALL_COUCHPOTATO_PWD_MD5=$(echo -n $INSTALL_PWD | openssl md5 | sed 's/^.* //' | tr -d '\n')
	log "COUCHPOTATO md5 password hash is $INSTALL_COUCHPOTATO_PWD_MD5"

	# Capture the sabnzbd api key
	export SABNZBD_API_KEY=$(sed -n 's/^api_key *= *\([^ ]*.*\)/\1/p' /home/$INSTALL_SABNZBD_USER/.sabnzbd/sabnzbd.ini)
	log "SABNZBD Api Key is $SABNZBD_API_KEY"

	# core configuration
    if [ $INSTALL_COUCHPOTATO_LOGINPAGE_ENABLE -eq 1 ]
    then
        set_section_name_value "core" "username" "$INSTALL_USER" /var/couchpotato/settings.conf
        set_section_name_value "core" "password" "$INSTALL_COUCHPOTATO_PWD_MD5" /var/couchpotato/settings.conf
    else
        set_section_name_value "core" "username" "" /var/couchpotato/settings.conf
        set_section_name_value "core" "password" "" /var/couchpotato/settings.conf
    fi

	set_section_name_value "core" "permission_folder" "$SMB_MOUNT_UMASK" /var/couchpotato/settings.conf
	set_section_name_value "core" "launch_browser" "0" /var/couchpotato/settings.conf
	set_section_name_value "core" "port" "$INSTALL_COUCHPOTATO_PORT" /var/couchpotato/settings.conf
	set_section_name_value "core" "permission_file" "$SMB_MOUNT_UMASK" /var/couchpotato/settings.conf
	set_section_name_value "core" "show_wizard" "0" /var/couchpotato/settings.conf

	# sabnzbd configuration
	set_section_name_value "sabnzbd" "enabled" "1" /var/couchpotato/settings.conf
	set_section_name_value "sabnzbd" "category" "movies" /var/couchpotato/settings.conf
	set_section_name_value "sabnzbd" "host" "$INSTALL_SABNZBD_HOST:$INSTALL_SABNZBD_PORT" /var/couchpotato/settings.conf
	set_section_name_value "sabnzbd" "api_key" "$SABNZBD_API_KEY" /var/couchpotato/settings.conf
	set_section_name_value "sabnzbd" "remove_complete" "1" /var/couchpotato/settings.conf

	# blackhole configuration
	set_section_name_value "blackhole" "enabled" "0" /var/couchpotato/settings.conf

	# kickasstorrents configuration
	set_section_name_value "kickasstorrents" "enabled" "0" /var/couchpotato/settings.conf

	# publichd configuration
	set_section_name_value "publichd" "enabled" "0" /var/couchpotato/settings.conf

	# manage configuration
	set_section_name_value "manage" "enabled" "1" /var/couchpotato/settings.conf

	# save default separator
	SAVEIFS=$IFS

	# set default separator to be a comma
	IFS=$(echo -en "\n\b")

	# loop through the genre folders within the mounted share for movies
	foldercount=0
	for foldername in $(find /home/$INSTALL_COUCHPOTATO_USER/mnt/$SMB_MOUNT_MOVIES_HOST/$SMB_MOUNT_MOVIES_SHARE -mindepth 1 -maxdepth 1 -type d -not -path '*/\.*')
	do
		((foldercount++))
		if [ $foldercount -eq 1 ]
		then
			folderlist="$foldername"
		else
			folderlist="$folderlist::$foldername"
		fi
	done

	set_section_name_value "manage" "library" "$folderlist" /var/couchpotato/settings.conf

	# restore default separator
	unset IFS

	# renamer configuration
	set_section_name_value "renamer" "enabled" "1" /var/couchpotato/settings.conf
	set_section_name_value "renamer" "from" "/mnt/$SMB_MOUNT_MDS_HOST/$SMB_MOUNT_MDS_SHARE/Downloads/complete" /var/couchpotato/settings.conf
	set_section_name_value "renamer" "to" "/mnt/$SMB_MOUNT_MOVIES_HOST/$SMB_MOUNT_MOVIES_SHARE/Download" /var/couchpotato/settings.conf
	set_section_name_value "renamer" "cleanup" "1" /var/couchpotato/settings.conf
	set_section_name_value "renamer" "folder_name" "" /var/couchpotato/settings.conf
	set_section_name_value "renamer" "file_name" " \<thename\> \[\<year\>\]\<cd\>.\<ext\>" /var/couchpotato/settings.conf
	set_section_name_value "renamer" "nfo_name" "\<filename\>.orig.\<ext\>" /var/couchpotato/settings.conf
	set_section_name_value "renamer" "file_action" "move" /var/couchpotato/settings.conf

	# subtitle configuration
	set_section_name_value "subtitle" "enabled" "1" /var/couchpotato/settings.conf
	set_section_name_value "subtitle" "languages" "en\, es" /var/couchpotato/settings.conf

	# searcher configuration
	set_section_name_value "searcher" "ignored_words" "german\, dutch\, truefrench\, danish\, swedish\, italian\, korean\, dubbed\, swesub\, korsub" /var/couchpotato/settings.conf
	set_section_name_value "searcher" "preferred_method" "nzb" /var/couchpotato/settings.conf

	# newznab configuration
	set_section_name_value "newznab" "use" "0,0,1,0,0,0" /var/couchpotato/settings.conf
	set_section_name_value "newznab" "enabled" "True" /var/couchpotato/settings.conf
	set_section_name_value "newznab" "api_key" ",,$NEWS_NZBS_ORG_API_KEY,,," /var/couchpotato/settings.conf

	log "Clearing CouchPotato logs"
	sudo rm -f /var/couchpotato/logs/*.log

	log "Starting CouchPotato"
	sudo service couchpotato start

	# Capture the CouchPotato api key
	export COUCHPOTATO_API_KEY=$(sed -n 's/^api_key *= *\([^ ]*.*\)/\1/p' /var/couchpotato/settings.conf)
	log "COUCHPOTATO Api Key is $COUCHPOTATO_API_KEY"

    log "Create a temporary directory for nzbToCouchPotato.py"
    cd $(mktemp -d)

    log "Setting up nzbToCouchPotato.py"
    git clone https://github.com/thorli/nzbToCouchPotato .
    mv autoProcessMovie.py nzbToCouchPotato.py /home/sabnzbd/scripts
    mv autoProcessMovie.cfg.sample /home/sabnzbd/scripts/autoProcessMovie.cfg

    set_section_name_value "CouchPotato" "host" "localhost" /home/sabnzbd/scripts/autoProcessMovie.cfg
    set_section_name_value "CouchPotato" "port" "$INSTALL_COUCHPOTATO_PORT" /home/sabnzbd/scripts/autoProcessMovie.cfg
    set_section_name_value "CouchPotato" "username" "$INSTALL_USER" /home/sabnzbd/scripts/autoProcessMovie.cfg
    set_section_name_value "CouchPotato" "password" "$INSTALL_COUCHPOTATO_PWD_MD5" /home/sabnzbd/scripts/autoProcessMovie.cfg
    set_section_name_value "CouchPotato" "ssl" "" /home/sabnzbd/scripts/autoProcessMovie.cfg
    set_section_name_value "CouchPotato" "web_root" "" /home/sabnzbd/scripts/autoProcessMovie.cfg
    set_section_name_value "CouchPotato" "apikey" "$COUCHPOTATO_API_KEY" /home/sabnzbd/scripts/autoProcessMovie.cfg
    set_section_name_value "CouchPotato" "delay" "60" /home/sabnzbd/scripts/autoProcessMovie.cfg
    set_section_name_value "CouchPotato" "method" "renamer" /home/sabnzbd/scripts/autoProcessMovie.cfg
fi

##################################################
# Sickbeard
##################################################

if [ $INSTALL_SICKBEARD_ENABLE -eq 1 ]
then
	log "Installing Sickbeard"

	log "Cloning Sickbeard to /opt/sickbeard"
	sudo git clone https://github.com/midgetspy/Sick-Beard.git /opt/sickbeard
	sudo chown -R $INSTALL_SICKBEARD_USER:$INSTALL_SICKBEARD_USER /opt/sickbeard

	log "Setup SickBeard as a service"
	sudo cp /opt/sickbeard/init.ubuntu /etc/init.d/sickbeard

	log "Create SickBeard data directory"
	sudo mkdir /var/sickbeard
	sudo chown -R $INSTALL_SICKBEARD_USER:$INSTALL_SICKBEARD_USER /var/sickbeard

	log "Create SickBeard run directory"
	sudo mkdir /var/run/sickbeard
	sudo chown -R $INSTALL_SICKBEARD_USER:$INSTALL_SICKBEARD_USER /var/run/sickbeard

	# Additional configuration appended to the /etc/default/sickbeard file
	(
		echo -e "SB_HOME=/opt/sickbeard";
		echo -e "SB_USER=$INSTALL_SICKBEARD_USER";
		echo -e "SB_DATA=/var/sickbeard";
		echo -e "SB_PIDFILE=/var/run/sickbeard/sickbeard.pid";
	) >> /etc/default/sickbeard
	sudo chmod 644 /etc/default/sickbeard

	log "Copying sabToSickBeard.py to /home/$INSTALL_SABNZBD_USER/scripts"
	sudo cp -p /opt/sickbeard/autoProcessTV/sabToSickBeard.py /home/$INSTALL_SABNZBD_USER/scripts
	sudo chown -R $INSTALL_SICKBEARD_USER:$INSTALL_USER_GROUP /home/$INSTALL_SABNZBD_USER/scripts/sabToSickBeard.py

	log "Copying autoProcessTV.py to /home/$INSTALL_SABNZBD_USER/scripts"
	sudo cp -p /opt/sickbeard/autoProcessTV/autoProcessTV.py /home/$INSTALL_SABNZBD_USER/scripts
	sudo chown -R $INSTALL_SICKBEARD_USER:$INSTALL_USER_GROUP /home/$INSTALL_SABNZBD_USER/scripts/autoProcessTV.py

	log "Set Sickbeard to run on boot"
	sudo update-rc.d sickbeard defaults

	##################################################
	# Sickbeard configuration
	##################################################
	log "Starting SickBeard (creates default configuration)"
	sudo service sickbeard start
	sleep 15s

	log "Stopping SickBeard"
	sudo service sickbeard stop

	log "Configuring SickBeard"
	backup /var/sickbeard/config.ini

	# Capture the sabnzbd api key
	export SABNZBD_API_KEY=$(sed -n 's/^api_key *= *\([^ ]*.*\)/\1/p' /home/$INSTALL_SABNZBD_USER/.sabnzbd/sabnzbd.ini)
	log "SABNZBD Api Key is $SABNZBD_API_KEY"

	# General configuration
    if [ $INSTALL_SICKBEARD_LOGINPAGE_ENABLE -eq 1 ]
    then
        set_section_name_value "General" "web_username" "$INSTALL_USER" /var/sickbeard/config.ini
        set_section_name_value "General" "web_password" "$INSTALL_PWD" /var/sickbeard/config.ini
    else
        set_section_name_value "General" "web_username" "" /var/sickbeard/config.ini
        set_section_name_value "General" "web_password" "" /var/sickbeard/config.ini
    fi

	set_section_name_value "General" "web_host" "$INSTALL_SICKBEARD_HOST" /var/sickbeard/config.ini
	set_section_name_value "General" "web_port" "$INSTALL_SICKBEARD_PORT" /var/sickbeard/config.ini
	set_section_name_value "General" "web_ipv6" "1" /var/sickbeard/config.ini
	set_section_name_value "General" "use_api" "1" /var/sickbeard/config.ini
	set_section_name_value "General" "nzb_method" "sabnzbd" /var/sickbeard/config.ini
	set_section_name_value "General" "quality_default" "164" /var/sickbeard/config.ini
	set_section_name_value "General" "provider_order" "sick_beard_index womble_s_index nzbs_org usenet_crawler omgwtfnzbs tvtorrents ezrss torrentleech btn hdbits" /var/sickbeard/config.ini
	set_section_name_value "General" "naming_pattern" "Season %S/%SN - S%0SE%0E - %EN" /var/sickbeard/config.ini
	set_section_name_value "General" "launch_browser" "0" /var/sickbeard/config.ini
	set_section_name_value "General" "metadata_wdtv" "0|0|0|0|1|0|0|0|0|0" /var/sickbeard/config.ini
	set_section_name_value "General" "root_dirs" "/mnt/$SMB_MOUNT_TV_HOST/$SMB_MOUNT_TV_SHARE" /var/sickbeard/config.ini
	set_section_name_value "General" "tv_download_dir" "/mnt/$SMB_MOUNT_MDS_HOST/$SMB_MOUNT_MDS_SHARE/Downloads/complete" /var/sickbeard/config.ini
	set_section_name_value "General" "keep_processed_dir" "0" /var/sickbeard/config.ini

	# SABnzbd configuration
	set_section_name_value "SABnzbd" "sab_apikey" "$SABNZBD_API_KEY" /var/sickbeard/config.ini
	set_section_name_value "SABnzbd" "sab_host" "http://localhost:$INSTALL_SABNZBD_PORT/" /var/sickbeard/config.ini

	# Newznab configuration
	set_section_name_value "Newznab" "newznab_data" "\"Sick Beard Index\|http://lolo.sickbeard.com/\|0\|5030,5040\|1\!\!\!NZBs.org\|http://nzbs.org/\|$NEWS_NZBS_ORG_API_KEY\|5030,5040,5070,5090\|1\!\!\!Usenet-Crawler\|http://www.usenet-crawler.com/\|$NEWS_USENET_CRAWLER_API_KEY\|5030,5040\|1\"" /var/sickbeard/config.ini

	##################################################
	# Autoprocesstv configuration
	##################################################

	log "Configuring autoProcessTV"
	sudo cp -p /opt/sickbeard/autoProcessTV/autoProcessTV.cfg.sample /opt/sickbeard/autoProcessTV/autoProcessTV.cfg

	set_section_name_value "SickBeard" "host" "$INSTALL_SICKBEARD_HOST" /opt/sickbeard/autoProcessTV/autoProcessTV.cfg
	set_section_name_value "SickBeard" "port" "$INSTALL_SICKBEARD_PORT" /opt/sickbeard/autoProcessTV/autoProcessTV.cfg
	set_section_name_value "SickBeard" "username" "$INSTALL_USER" /opt/sickbeard/autoProcessTV/autoProcessTV.cfg
	set_section_name_value "SickBeard" "password" "$INSTALL_PWD" /opt/sickbeard/autoProcessTV/autoProcessTV.cfg
	set_section_name_value "SickBeard" "web_root" "" /opt/sickbeard/autoProcessTV/autoProcessTV.cfg
	set_section_name_value "SickBeard" "ssl" "0" /opt/sickbeard/autoProcessTV/autoProcessTV.cfg

	log "Removing ^M characters from autoProcessTV.cfg"
	sudo dos2unix /opt/sickbeard/autoProcessTV/autoProcessTV.cfg

	log "Copying autoProcessTV.cfg to /home/$INSTALL_SABNZBD_USER/scripts"
	sudo cp -p /opt/sickbeard/autoProcessTV/autoProcessTV.cfg /home/$INSTALL_SABNZBD_USER/scripts
	sudo chown -R $INSTALL_SICKBEARD_USER:$INSTALL_USER_GROUP /home/$INSTALL_SABNZBD_USER/scripts/autoProcessTV.cfg

	log "Clearing SickBeard logs"
	sudo rm -f /opt/sickbeard/Logs/*.log

	log "Starting SickBeard"
	sudo service sickbeard start
fi

##################################################
# Headphones
##################################################

if [ $INSTALL_HEADPHONES_ENABLE -eq 1 ]
then
	log "Installing Headphones"
	sudo git clone https://github.com/rembo10/headphones.git /opt/headphones
	sudo chown -R $INSTALL_HEADPHONES_USER:$INSTALL_HEADPHONES_USER /opt/headphones

	log "Setup Headphones as a service"
	sudo cp /opt/headphones/init-scripts/init.ubuntu /etc/init.d/headphones

	log "Create Headphones data directory"
	sudo mkdir /var/headphones
	sudo chown -R $INSTALL_HEADPHONES_USER:$INSTALL_HEADPHONES_USER /var/headphones

	# Additional configuration appended to the /etc/default/headphones file
	(
		echo -e "HP_HOME=/opt/headphones";
		echo -e "HP_USER=$INSTALL_HEADPHONES_USER";
		echo -e "HP_DATA=/var/headphones";
		echo -e "HP_PIDFILE=/var/run/headphones/headphones.pid";
	) >> /etc/default/headphones
	sudo chmod 644 /etc/default/headphones

	log "Set Headphones to run on boot"
	sudo update-rc.d headphones defaults

	##################################################
	# Headphones configuration
	##################################################
	log "Starting Headphones (creates default configuration after stop)"
	sudo service headphones start
	sleep 15s

	log "Stopping Headphones"
	sudo service headphones stop

	log "Configuring Headphones"
	backup /var/headphones/config.ini

	# Capture the sabnzbd api key
	export SABNZBD_API_KEY=$(sed -n 's/^api_key *= *\([^ ]*.*\)/\1/p' /home/$INSTALL_SABNZBD_USER/.sabnzbd/sabnzbd.ini)
	log "SABNZBD Api Key is $SABNZBD_API_KEY"

	# General configuration
    if [ $INSTALL_HEADPHONES_LOGINPAGE_ENABLE -eq 1 ]
        set_section_name_value "General" "http_username" "$INSTALL_USER" /var/headphones/config.ini
        set_section_name_value "General" "http_password" "$INSTALL_PWD" /var/headphones/config.ini
    then
        set_section_name_value "General" "http_username" "" /var/headphones/config.ini
        set_section_name_value "General" "http_password" "" /var/headphones/config.ini
    fi

	set_section_name_value "General" "http_host" "$INSTALL_HEADPHONES_HOST" /var/headphones/config.ini
	set_section_name_value "General" "http_port" "$INSTALL_HEADPHONES_PORT" /var/headphones/config.ini
	set_section_name_value "General" "destination_dir" "/mnt/$SMB_MOUNT_MUSIC_HOST/$SMB_MOUNT_MUSIC_SHARE" /var/headphones/config.ini
	set_section_name_value "General" "move_files" "1" /var/headphones/config.ini
	set_section_name_value "General" "rename_files" "1" /var/headphones/config.ini
	set_section_name_value "General" "cleanup_files" "1" /var/headphones/config.ini
	set_section_name_value "General" "nzb_downloader" "0" /var/headphones/config.ini
	set_section_name_value "General" "launch_browser" "0" /var/headphones/config.ini
	set_section_name_value "General" "download_dir" "/mnt/$SMB_MOUNT_MDS_HOST/$SMB_MOUNT_MDS_SHARE/Downloads/complete" /var/headphones/config.ini
	set_section_name_value "General" "keep_processed_dir" "0" /var/headphones/config.ini

	# SABnzbd configuration
	set_section_name_value "SABnzbd" "sab_host" "http://localhost:$INSTALL_SABNZBD_PORT/" /var/headphones/config.ini
	set_section_name_value "SABnzbd" "sab_apikey" "$SABNZBD_API_KEY" /var/headphones/config.ini
	set_section_name_value "SABnzbd" "sab_category" "music" /var/headphones/config.ini

	# NZBsorg configuration
	set_section_name_value "NZBsorg" "nzbsorg" "1" /var/headphones/config.ini
	set_section_name_value "NZBsorg" "nzbsorg_hash" "$NEWS_NZBS_ORG_API_KEY" /var/headphones/config.ini

	log "Clearing Headphones logs"
	sudo rm -f /var/headphones/logs/*.log

	log "Starting Headphones"
	sudo service headphones start
fi

##################################################
# Index page
##################################################

if [ $INSTALL_INDEXPAGE_ENABLE -eq 1 ]
then

    if [ -d "$INSTALL_INDEXPAGE_PATH" ]
    then
        if [ -f "$INSTALL_INDEXPAGE_PATH/index.html" ]
        then
            backup $INSTALL_INDEXPAGE_PATH/index.html
            sudo chmod 646 $INSTALL_INDEXPAGE_PATH/index.html
        fi
    else
        log "Create $INSTALL_INDEXPAGE_PATH"
        sudo mkdir -p $INSTALL_INDEXPAGE_PATH
    fi

	log "Copying application images to $INSTALL_INDEXPAGE_PATH"
	if [ $INSTALL_SABNZBD_ENABLE -eq 1 ]
	then
		sudo cp -p /usr/share/sabnzbdplus/interfaces/smpl/templates/static/sabnzbd_small4.png $INSTALL_INDEXPAGE_PATH
	fi
	if [ $INSTALL_COUCHPOTATO_ENABLE -eq 1 ]
	then
		sudo cp -p /opt/couchpotato/couchpotato/static/images/couch.png $INSTALL_INDEXPAGE_PATH
	fi
	if [ $INSTALL_SICKBEARD_ENABLE -eq 1 ]
	then
		sudo cp -p /opt/sickbeard/data/images/sickbeard.png $INSTALL_INDEXPAGE_PATH
	fi
	if [ $INSTALL_HEADPHONES_ENABLE -eq 1 ]
	then
		sudo cp -p /opt/headphones/data/images/headphoneslogo.png $INSTALL_INDEXPAGE_PATH
	fi

	log "Build index page $INSTALL_INDEXPAGE_PATH/index.html"
	(
		echo -e "<!DOCTYPE html>"
		echo -e "<html>"
		echo -e "<head>"
		echo -e "<title>$HOSTNAME</title>"
		echo -e "</head>"
		echo -e "<body>"
		if [ $INSTALL_SABNZBD_ENABLE -eq 1 ]
		then
			echo -e "<p>$INSTALL_SABNZBD_TITLE<a href=\"http://$HOSTNAME:$INSTALL_SABNZBD_PORT\">"
			echo -e "<img src=\"sabnzbd_small4.png\" alt=\"Sabnzbdplus\" width=\"225\" height=\"47\"></a></p>"
		fi
		if [ $INSTALL_COUCHPOTATO_ENABLE -eq 1 ]
		then
			echo -e "<p>$INSTALL_COUCHPOTATO_TITLE<a href=\"http://$HOSTNAME:$INSTALL_COUCHPOTATO_PORT\">"
			echo -e "<img src=\"couch.png\" alt=\"CouchPotato\" width=\"100\" height=\"100\"></a></p>"
		fi
		if [ $INSTALL_SICKBEARD_ENABLE -eq 1 ]
		then
			echo -e "<p>$INSTALL_SICKBEARD_TITLE<a href=\"http://$HOSTNAME:$INSTALL_SICKBEARD_PORT\">"
			echo -e "<img src=\"sickbeard.png\" alt=\"SickBeard\" width=\"150\" height=\"72\"></a></p>"
		fi
		if [ $INSTALL_HEADPHONES_ENABLE -eq 1 ]
		then
			echo -e "<p>$INSTALL_HEADPHONES_TITLE<a href=\"http://$HOSTNAME:$INSTALL_HEADPHONES_PORT\">"
			echo -e "<img src=\"headphoneslogo.png\" alt=\"Headphones\" width=\"64\" height=\"64\"></a></p>"
		fi
		echo -e "</body>"
		echo -e "</html>"
	) > $INSTALL_INDEXPAGE_PATH/index.html
	sudo chmod 644 /etc/fstab

	log "Downloading $HOSTNAME icon"
	sudo wget -U Mozilla/5.0 -O $INSTALL_INDEXPAGE_PATH/favicon.ico $INSTALL_INDEXPAGE_ICON_URL
fi

##################################################
# Handbrake
##################################################

if [ $INSTALL_HANDBRAKE_ENABLE -eq 1 ]
then
    if [ $INSTALL_RASPBIAN -eq 1 ]
    then
        log "Installing Handbrake-CLI dependencies"
        sudo DEBIAN_FRONTEND=noninteractive apt-get -y install autoconf automake build-essential cmake git libass-dev libbz2-dev libfontconfig1-dev libfreetype6-dev libfribidi-dev libharfbuzz-dev libjansson-dev libmp3lame-dev libogg-dev libopus-dev libsamplerate-dev libtheora-dev libtool libtool-bin libvorbis-dev libx264-dev libxml2-dev m4 make patch pkg-config python tar yasm zlib1g-dev

        log "Create a temporary directory for Handbrake-CLI"
        cd $(mktemp -d)

        log "Downloading Handbrake source"
        git clone https://github.com/HandBrake/HandBrake.git && cd HandBrake
        
        mkdir /home/$INSTALL_USER/bin
        log "Force gcc and g++ to use the Raspberry Pi3 CPU"
        (
            echo -e "#!/bin/sh"
            echo -e "exec /usr/bin/gcc "$@" -mcpu=cortex-a53 -mfpu=neon-vfpv4"
        ) > /home/$INSTALL_USER/bin/gcc

        (
            echo -e "#!/bin/sh"
            echo -e "exec /usr/bin/g++ "$@" -mcpu=cortex-a53 -mfpu=neon-vfpv4"
        ) > /home/$INSTALL_USER/bin/gcc
        chmod +x /home/$INSTALL_USER/bin/g++ ~/bin/gcc
        export PATH=/home/$INSTALL_USER/bin:$PATH

        log "Building HandBrakeCLI from source"
        ./configure --launch-jobs=$(nproc) --launch --disable-gtk

        log "Copying HandBrakeCLI to /opt/HandBrake"
        sudo mkdir /opt/HandBrake
        sudo cp -p build/HandBrakeCLI /opt/HandBrake

        log "Setting ownership and permissions"
        sudo chown -R sabnzbd:media /opt/HandBrake
        sudo chmod +x /opt/HandBrake/HandBrakeCLI
    else
        log "Installing Ubuntu Handbrake"
        sudo DEBIAN_FRONTEND=noninteractive apt-get -y install handbrake-cli libdvdread4 libdvdcss

        log "Update libdvdread"
        sudo DEBIAN_FRONTEND=noninteractive apt-get update
        sudo DEBIAN_FRONTEND=noninteractive apt-get -y dist-upgrade
        sudo /usr/share/doc/libdvdread4/install-css.sh
    fi
fi

##################################################
# UFW
##################################################

if [ $INSTALL_UFW_ENABLE -eq 1 ]
then
	log "Installing Uncomplicated Firewall"
	sudo DEBIAN_FRONTEND=noninteractive apt-get -y install ufw

	backup /etc/default/ufw

	# Set default firewall rules
    sudo ufw default deny incoming
    sudo ufw default allow outgoing

	# Allow SSH connections only from within the LAN, if selected (note: if running on the cloud, this will prevent SSH access to the server!)
	if [ $INSTALL_UFW_LANONLY -eq 1 ]
	then
		sudo ufw allow from $INSTALL_UFW_LANONLY_NETPREFIX to any port ssh
	fi

	# Allow Sabnzbd port
	if [ $INSTALL_SABNZBD_ENABLE -eq 1 ]
	then
		sudo ufw allow from any to any port $INSTALL_SABNZBD_PORT proto tcp
	fi

	# Allow Couchpotato port
	if [ $INSTALL_COUCHPOTATO_ENABLE -eq 1 ]
	then
		sudo ufw allow from any to any port $INSTALL_COUCHPOTATO_PORT proto tcp
	fi

	# Allow Sickbeard port
	if [ $INSTALL_SICKBEARD_ENABLE -eq 1 ]
	then
		sudo ufw allow from any to any port $INSTALL_SICKBEARD_PORT proto tcp
	fi

	# Allow Headphones port
	if [ $INSTALL_HEADPHONES_ENABLE -eq 1 ]
	then
		sudo ufw allow from any to any port $INSTALL_HEADPHONES_PORT proto tcp
	fi

    if [ $NGINX_ENABLE -eq 1 ]
    then
        sudo ufw allow "$NGINX_UFW_PROFILE"
    fi

	# Enable UFW
	sudo ufw --force enable

	# Display firewall status and rules
	sudo ufw status verbose
fi

##################################################
# cron
##################################################

if [ $INSTALL_CRON_ENABLE -eq 1 ]
then
	log "Setting crontab to restart the server at the start of each month"
	(sudo crontab -l 2>/dev/null; echo "# Restart the server each month") | sudo crontab -
	(sudo crontab -l 2>/dev/null; echo "* * 1 * * /usr/sbin/shutdown -r now") | sudo crontab -

	log "Setting crontab to remount the drive mappings every 5 minutes"
	(sudo crontab -l 2>/dev/null; echo "# Remount the drive mappings") | sudo crontab -
	(sudo crontab -l 2>/dev/null; echo "*/5 * * * * /bin/mount -a") | sudo crontab -

	# log "Setting crontab (as user sabnzbd) to delete files from /var/media/Downloads when the disk is full"
	log "Setting crontab (as user sabnzbd) to delete files from /mnt/$SMB_MOUNT_MDS_HOST/$SMB_MOUNT_MDS_SHARE/Downloads when the disk is full"
	# (sudo crontab -u sabnzbd -l 2>/dev/null; echo "# Delete files from /var/media/Downloads when the disk is full") | sudo crontab -u sabnzbd -
	(sudo crontab -u sabnzbd -l 2>/dev/null; echo "# Delete files from /mnt/$SMB_MOUNT_MDS_HOST/$SMB_MOUNT_MDS_SHARE/Downloads when the disk is full") | sudo crontab -u sabnzbd -
	(sudo crontab -u sabnzbd -l 2>/dev/null; echo "0 1 * * * /home/sabnzbd/bin/remove_downloads_disk_full.sh") | sudo crontab -u sabnzbd -
fi
