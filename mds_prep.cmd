# MDS Server command file for script preparation

# Go to the home directory
cd ~

# Go to the home directory
cd ~

# Convert files to unix format
for f in *.sh *.conf *.cfg *.sql *.txt *.py *rsa *.pub *hosts
do
	if [ -f $f ]
	then
		echo "Converting $f to unix format..."
		vi +':w ++ff=unix' +':q' $f 2>/dev/null
        
        echo "Removing any ^M characters (if they still exist) from $f..."
        tr -d $'\r' < $f > $f.tmp
        mv $f.tmp $f
	fi
done

# Assign execute permissions for scripts
chmod +x *.sh
