#!/bin/bash

#####################
# PARAMETERS
####################

# Verbose mode. For debugging only.
VERBOSE=false

# Exit states.
STATE_OK=0
STATE_WARNING=1
STATE_CRITICAL=2
STATE_UNKNOWN=3

# Video options
#VIDEO_FORMAT=mkv
VIDEO_BITRATE=3000
VIDEO_ENCODER=x264
VIDEO_DEINTERLACE_LEVEL=slow
VIDEO_DENOISE_LEVEL=medium

# Audio options
AUDIO_ENCODER=copy:ac3
AUDIO_BITRATE=160
AUDIO_SAMPLERATE=48

# Usage defaults.
USAGE="Usage: `basename $0` [-h] [-v] inputfile [domain]"
USAGE_HELP="Convert a file using HandBrake.\n
Example: `basename $0` -p -v infile.avi mkv\n
\n
OPTIONS\n
-h    display help\n
-v    verbose mode"

# Check the input parameters.
if [ $# -eq 0 ] ; then
	echo $USAGE 1>&2
	echo -e $USAGE_HELP
	exit $STATE_UNKNOWN
else
	# Get the command line options and their arguments.
	while getopts hfv OPTIONS ; do
		case $OPTIONS in
		h)	echo $USAGE 1>&2
			echo -e $USAGE_HELP
			exit $STATE_UNKNOWN
		;;
		v)  VERBOSE=true
		;;
		*)  echo $USAGE 1>&2
			exit $STATE_UNKNOWN
		;;
		esac
	done

	# Shift the parameters to the start of the list so that can be accessed.
	shift $(($OPTIND-1))

	# Note: The input file MUST be the first parameter! The second parameter (format) is optional.
	case $# in
	1)	# Only one parameter supplied.
		INFILE=$1
	;;
	#2)	# Two parameters supplied.
	#	INFORMAT=$2
	#;;
	*)	echo $USAGE 1>&2
		exit $STATE_UNKNOWN
	;;
	esac
fi

INFORMAT=mkv
VIDEO_FORMAT_UPPER=`echo $INFORMAT | tr '[:lower:]' '[:upper:]'`
VIDEO_FORMAT_LOWER=`echo $INFORMAT | tr '[:upper:]' '[:lower:]'`

# Remove the whitespace and replace with underscores
#FILENAME_FULL=$INFILE
FILENAME_FULL=$(echo $INFILE | sed -e 's/ /_/g')
FILENAME_BASE=$(echo ${FILENAME_FULL%.*})

echo "Converting $INFILE to $VIDEO_FORMAT_UPPER format"
echo "Destination file: /tmp/$FILENAME_BASE.$VIDEO_FORMAT_LOWER"

# Create a temporary version of the input file
cp -p "$INFILE" /tmp/
mv /tmp/"$INFILE" /tmp/$FILENAME_FULL

##################################################
# HandBrake conversion
##################################################

/usr/bin/HandBrakeCLI -i /tmp/$FILENAME_FULL -o /tmp/$FILENAME_BASE.$VIDEO_FORMAT_LOWER -f $VIDEO_FORMAT_LOWER -e $VIDEO_ENCODER -b $VIDEO_BITRATE -E $AUDIO_ENCODER -B $AUDIO_BITRATE -R $AUDIO_SAMPLERATE -d $VIDEO_DEINTERLACE_LEVEL -5 -9 $VIDEO_DENOISE_LEVEL

####################
# FINISH
####################

echo "Conversion complete."
exit $STATE_OK
