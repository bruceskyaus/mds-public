@echo off

rem BASH script to execute HandBrakeCLI with pre-defined parameters
rem Input: The script expects a directory that contains large movie files that require shrinking. Only files greater than the specified size will be compressed.
rem Output: MKV video files, in x264 AC3 format
rem 29/03/2016

set title=The Intern [2015]
set inputdir=Z:\Download\
set inputfile=The Intern [2015].img
set outputdir=E:\Temp\CompressedMovies\
set filebasename=%title%
set filesuffix=mkv
set logfile="E:\Temp\CompressedMovies\handbrake.log"
set videoquality=25

HandBrakeCLI --input "%inputdir%%inputfile%" --output "%outputdir%%filebasename%.%filesuffix%" --title 0 --preset Normal --no-dvdnav --format av_mkv --encoder x264 --encoder-preset fast --quality $videoquality --aencoder copy:ac3 --audio-fallback ac3 --mixdown 5point1 --native-language eng --native-dub --subtitle-default 1 > %logfile% 2>&1