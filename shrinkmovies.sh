#/bin/bash

# Execute the shrinkvideo.bash script in the background

# Bruce Smith
# 17/04/2016

# Movies over 2G
nohup ./shrinkvideo.bash -i "/mnt/titan/WDTVLiveHub" -o "/mnt/titan/WDTVLiveHub/Download" -s "+2G" -q 25 > /tmp/shrinkvideo.log 2>&1 < /dev/null &