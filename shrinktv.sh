#/bin/bash

# Execute the shrinkvideo.bash script in the background

# Bruce Smith
# 17/04/2016

# TV over 500K
nohup ./shrinkvideo.bash -i "/mnt/titan/RecentTV" -o "/mnt/titan/RecentTV" -s "+500K" -q 25 > /tmp/shrinkvideo.log 2>&1 < /dev/null &
