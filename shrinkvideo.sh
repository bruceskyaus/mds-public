#!/bin/bash

# BASH script to execute HandBrakeCLI with pre-defined parameters
# Input: The script expects a directory that contains large movie files that require shrinking. Only files greater than the specified size will be compressed (one per exection of this script, largest first).
# Output: MKV video files, in x264 AC3 format

# Bruce Smith
# 21/04/2016

# Note: The user executing this script must have write permissions to the output directory! The user in the MDS is sabnzbd.

TEMPDIR=/tmp
FILESUFFIX=mkv
LOGFILE="Handbrake.log"

# Usage defaults.
USAGE="Usage: `basename $0` [-h] -i inputdir -o outputdir -s minsize -q quality"
USAGE_HELP="Loop through the files and subdirectories of the input directory and compress video files over a predefined size.\n
Example: `basename $0` -i /mnt/myinputdir -o /mnt/myoutputdir -s 2G -q 25\n
\n
OPTIONS\n
-h\tdisplay help\n
-i\tinput directory\n
-o\toutput directory\n
-s\tminimum size\n
-q\tvideo quality"

# Check the input parameters.
if [ $# -eq 0 ] ; then
	echo $USAGE 1>&2
	echo -e $USAGE_HELP
	exit 1
else
	# Get the command line options and their arguments.
	while getopts hi:o:s:q: OPTIONS ; do
		case $OPTIONS in
		h)	echo $USAGE 1>&2
			echo -e $USAGE_HELP
			exit 1
		;;
		i)  INPUTDIR="$OPTARG"
		;;
		o)	OUTPUTDIR="$OPTARG"
		;;
		s)	MINSIZE="$OPTARG"
		;;
		q)	VIDEOQUALITY="$OPTARG"
		;;
		*)  echo $USAGE 1>&2
			exit 1
		;;
		esac
	done
fi

# Ensure that the input and output directories have a slash on the end
INPUTDIR="${INPUTDIR%/}/"
OUTPUTDIR="${OUTPUTDIR%/}/"

echo "Input parameters."
echo "	INPUTDIR=$INPUTDIR"
echo "	OUTPUTDIR=$OUTPUTDIR"
echo "	MINSIZE=$MINSIZE"
echo "	VIDEOQUALITY=$VIDEOQUALITY"

# Set the title (always 1 for movie files)
title=1

# Loop through the files and apply the HandBrake compression
shopt -s dotglob
find "$INPUTDIR" -size "$MINSIZE" | sort -nr | head -1 | while read f; do 

	# Display the file being processed
    ls -sh "$f"

	# Extract the filename and lowercase extension
	filedir=$(dirname "$f")
	filebasename=$(basename "$f")
	fileprefix="${filebasename%.*}"
	fileext=$(echo "${filebasename##*.}" | tr "[:upper:]" "[:lower:]")

	# echo "DEBUG: filebasename=$filebasename"
	# echo "DEBUG: fileprefix=$fileprefix"
	# echo "DEBUG: fileext=$fileext"
	# echo "DEBUG: filedir=$filedir"
	
	# Mount img or iso files as directories
	case "$fileext" in
		"img")
			echo "DEBUG: Mounting IMG file..."
			
			cd /tmp
			mkdir img
			mount -t udf "$f" /tmp/img -o loop
			inputdir=/tmp/img
			inputfile=""
		;;
		"iso")
			echo "DEBUG: Mounting ISOS file..."
			cd /tmp
			mkdir iso
			mount "$f" /tmp/iso/ -t iso9660 -o ro,loop=/dev/loop0			
			inputdir=/tmp/iso
			inputfile=""
		;;
		*)
			inputdir=$filedir
			inputfile=$filebasename
		;;
	esac

	echo "DEBUG: Executing HandBrakeCLI..."	
    # Execute handbrake CLI using the input parameters. Redirect all stdout and stderr output to stdout.
	nice -n 19 HandBrakeCLI --input "$inputdir/$inputfile" --output "$TEMPDIR/$fileprefix.$FILESUFFIX" --title $title --preset Normal --no-dvdnav --format av_mkv --encoder x264 --encoder-preset fast --quality $VIDEOQUALITY --aencoder copy:ac3 --audio-fallback ac3 --mixdown 5point1 --native-language eng --native-dub --subtitle-default 1 2>&1 | tr '\r' '\n' > "$TEMPDIR/$LOGFILE" 
	
	if [ $? -eq 0 ]; then
		echo "HandBrakeCLI completed successfully"
		
		# Unmount and remove img or iso mount points
		case "$fileext" in
			"img")
				echo "DEBUG: Unmount img mount point..."
				umount /tmp/img
				rmdir img
			;;
			"iso")
				echo "DEBUG: Unmount iso mount point..."
				umount /tmp/iso
				rmdir iso
			;;
		esac
			
		if [ -e "$TEMPDIR/$fileprefix.$FILESUFFIX" ] ; then
			echo "DEBUG: Copying $TEMPDIR/$fileprefix.$FILESUFFIX to $OUTPUTDIR..."
			# Copy the file from the temp directory to the output directory (verbose, noclobber)
			cp -vn "$TEMPDIR/$fileprefix.$FILESUFFIX" "$OUTPUTDIR$fileprefix.$FILESUFFIX"

			# Only delete the temporary file after it has been successfully copied
			if [ $? -eq 0 ]; then
				echo "DEBUG: Deleting $TEMPDIR/$fileprefix.$FILESUFFIX..."
				# Delete the temporary file
				rm -f "$TEMPDIR/$fileprefix.$FILESUFFIX"	
			else
				echo "Copy of temporary file $TEMPDIR/$fileprefix.$FILESUFFIX to $OUTPUTDIR$fileprefix.$FILESUFFIX failed."
				exit 1
			fi
		else
			echo "Temporary file $TEMPDIR/$fileprefix.$FILESUFFIX does not exist. Exiting."
			exit 1
		fi
	else
		echo "HandBrakeCLI exited abnormally"
		exit 1
	fi
done
